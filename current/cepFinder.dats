cepFinder.dats

Pfad:     <<Rule Package Name>>/<<Release>>/<<Rule Group>>/<<Rule>>/
Präsenz:  optional
Referenz: -
Inhalte:  P23R Selection Skript zur Ermittlung der notwendigen Informationenen über einen Kommunikationsendpunkt

# Communication End Point Finder

This script is only required if a notification rule request the communication
parameters for a communication end point of a notification receiver from the
public source data connector of the public control centre to transmit a
notification. The script should be activated by calling it as one of the
generation steps, which are defined in the manifest of a notification rule.
Typically, it should be in the last step. This script requires that there
is a minimum of one receiverId given in the communication section of the
notification profile.

This P23R selection script copies the current notification profile from _pipe
in_ to _pipe out_ and complements the channel information of the receivers, which
are need to transmit the notification using the related communication connector.
The script expect the following fields to be given in the notification profile:

* notificationProfile/communication/receivers/receiverId
* notificationProfile/communication/receivers/receiverName
* notificationProfile/communication/receivers/support
* notificationProfile/communication/receivers/techniscalSupport
* notificationProfile/communication/criteria

where there should be one or more criteria/name equal to "channel.service" to
identify the requested channels.

_Cep_ is the model describing the communication end points (cep) for all
receivers of notifications registered at the public p23r control centre.

	Model Cep = http://leitstelle.p23r.de/NS/p23r/nrl/CommunicationEndPoint1-0

	NotificationProfile {
		@ns { "http://leitstelle.p23r.de/NS/p23r/nrl/NotificationProfile1-1" }
		[

Take the existing notification profile.

			for each np
			take 1

Extract the service names of the channels to be used.

			let services = [
				for criteria in np.communication.criteria
				where criteria.name == "channel.service"
				return criteria@
			]

Get the receiver information with its communication end point ids remote
from the control centre. 

			let cepReceivers = [
				parameter {
					[
						for each r in np.communication.receivers
						return receiverIds { r@receiverId }
					]
				}
				
				for each parameter
				for each r in parameter.receiverIds
				for each receiver in Cep.receivers
				where receiver.receiverId == r@receiverId
				return receiver
			]

Validate locally the receiver content so we get a valid list of CEP channel ids.

			checkSignatures(
				receiver.signedCeps,
				"the list of CEP channel ids should be valid signed"
			)

Create the receiver information for all receiver containing their related
channel information.

			let receivers = [
				for each cr in cepReceivers

Get the communication end point information for all CEP channel ids remote
from the control centre.

				let signedChannels = [
					parameter {
						[
							for each cep in cr.signedCeps.content
							for each service in services
							where cep.service == service
							for each id in cep.channels
							return cepIds { id }
						]
					}
					
					for each parameter
					for each id in parameter.cepIds
					for each channel in Cep.signedChannels
					where channel.content.id == id
					return channel
				]

Validate locally the cep content so we get a valid list of communication
channels, which we can use for the transmission of the notification.

				checkSignatures(
					signedChannels,
					"the content of the CEP channels should be valid signed"
				)

Create the channel information as required in the notification profile.

				let channels = [
					for each channel in signedChannels
					return channels {
						type { channel.content.type }
						id { channel.content.id }
						name { channel.content.name }
						priority { channel.content.priority }
						parameters { channel.content.parameters }
					}
				]

Create the receiver information as required in the notification
profile.

				return receivers {
					@receiverId { cr@receiverId },
					@receiverName { cr@receiverName },
					support { cr.support },
					technicalSupport { cr.technicalSupport }
					channels
				}
			]

Rebuild the communication section.

			let communication = communication {
				ng.notificationProfile.communication.publicTimeStamp
				receivers
			}

Add the new content to the given information of the old notification
profile and create a new notification profile.

			return @tenant { ng.notificationProfile@tenant },
				ng.notificationProfile.General,
				ng.notificationProfile.Signatures,
				communication,
				ng.notificationProfile.GenerationLog,
				ng.notificationProfile.Extension,
				ng.notificationProfile.P23rExtension
		]
	}
