## Specification of the Rule Package Archive Structure

### _p23r.nrl.RulePackageStructure_

This specification describes the file and directory structure of the rule packages, which is used to generate the archive file (zip) of a rule package.

#### Notes

_none_

#### Specification

The rule package includes all notification rule groups with its notification rules.

This directory must be the unique name of the rule package as given in the manifest of the rule package.

	«RulePackageName»

This directory must be the unique release number of the rule package as given in the manifest
of the rule package.

		«Release»

This optional directory can contain shared files. The optional directory sources contain
the source files of the shared directory, if the source file is written in one of the optional F-BRS languages.

			shared → optional
				sources → optional

This optional directory contain the source files, if the source file is written in one of the optional
F-BRS languages.

			sources → optional

The manifest of the rule package contains its meta information regarding especially the packing and
publishing aspects of test package.

			Manifest.xml

The optional recommendation scripts will be executed to provide a recommendation to active the rule package
depending of the source data from the principal. One of the recommendation scripts must be present. If both
scripts are present the P23R Selection script must be executed first.

			Recommendation.dats → optional
			Recommendation.xslt → optional

Each notification rule group have its own subdirectory. The name of the directory must be the unique name of the
notification rule group as given in the manifest of the notification rule group. The names _shared_ and _sources_
are reserved.

			«RuleGroupName» → many

This optional directory can contain shared files. The optional directory sources contain
the source files of the shared directory, if the source file is written in one of the optional F-BRS languages.

				shared → optional
					sources → optional

This optional directory contain the source files, if the source file is written in one of the optional
F-BRS languages.

				sources → optional

The manifest of the notification rule group contains its meta information regarding especially the administrative
information and overall aspects of the containing notification rules.

				Manifest.xml

The optional recommendation scripts will be executed to provide a recommendation to active the rule group
depending of the source data from the principal. One of the recommendation scripts must be present. If both scripts
are present the P23R Selection script must be executed first.

				Recommendation.dats → optional
				Recommendation.xslt → optional

The optional configuration scripts will be executed to provide precalculated values
depending of the source data from the principal. If both scripts are present the P23R Selection script must
be executed first.

				Configuration.dats → optional
				Configuration.xslt → optional

Each notification rule  have its own subdirectory. The name of the directory must be the unique name of the
notification rule as given in the manifest of the notification rule. The names _shared_ and _sources_ are reserved.

				«RuleName» → many

This optional directory can contain shared files. The optional directory sources contain
the source files of the shared directory, if the source file is written in one of the optional F-BRS languages.

					shared → optional
						sources → optional

This optional directory contain the source files, if the source file is written in one of the optional
F-BRS languages.

					sources → optional

The manifest of the notification rule contains its meta information regarding especially the administrative
information of the notification rule.

					Manifest.xml

This schema defines the format of the incoming messages. The namespace is used to identify to which
rule an incoming message belongs.

					Message.xsd

These scripts select the required source data and generate one or more initial notification profiles. The incoming
message anf further meta data is provided as input. If both scripts are present the P23R Selection script must be
executed first.

					MultiNotificationProfile.dats → optional
					MultiNotificationProfile.xslt → optional

Each step of the notification generation creates a temporary result, which must match with one of the given
schemata.

					«Notification Type».xsd → optional multiple

The final generation step must create a notification which matches this schema. The notification is provided
to the principal for final acceptance.

					Notification.xsd

Each generation step includes the conversion of the notification itself as well as the conversion of the
complementary profile with the meta information. The conversion can be implemented by a XSLTv2 script or
a P23R Selection script. Each generation step must include «Notification Selection».dats or
«Notification Transformation».xslt .

					«Notification Selection».dats → optional (only if .xslt are present) multiple
					«Notification Transformation».xslt → optional (only if .dats are present) multiple
					
					«Profile Selection».dats → optional multiple
					«Profile Transformation».xslt → optional multiple

This script filters the final notification and creates a signature for the resulting XML document. The signature
is used to identify changes in the core data of the generated notification.

					NotificationCore.xslt → optional

This script can be used by a P23R client to convert the final notification into a HTML document, which is shown
in a pretty form to the user using a common style sheet with given CSS3 classes.

					NotificationView.xslt → optional

Depending of the identified channel type and target, which is used to send the final notification, the script and
the assets in the related directory are provided together with the accepted, signed notification to the target
communication connector.

					Representation-«Channel Type»-«Target».xslt → optional multiple
					Representation-«Channel Type»-«Target»      → optional multiple

This document is a readable, formated ebook with all commented source files of a notification rule.

					Source.epub → optional
