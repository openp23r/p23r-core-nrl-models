### Specification of the Test Package Manifest

#### _p23r.nrl.TestPackageManifest_

This specification describes the syntax of the test package manifest, which is the core of a test package. This specification is normative for P23R solutions.

##### Notes

_none_

##### Specification

The test package manifest is part of the core specifications for the P23R and belongs to the technical notification rule language (T-BRS). Therefore it is maintained by the p23r core team.

	model p23r.nrl.TestPackageManifest as tpm release 1.0 {
	
		creator 'Jan Gottschick'
		organisation 'P23R core team'
		base http://leitstelle.p23r.de/NS/
		requires p23r release 1.1
		
		title 'Manifest format for P23R test package'
		
		tag 'test'
		tag 'package'
		tag 'manifest'
		tag 'p23r core'
		
		description 'The manifest describes the meta information of a test package.'
		
		namespace cc=http://leitstelle.p23r.de/NS/p23r/nrl/Common1-0 file 'Common1-0.xsd'

The manifest describes the overall meta information required in the publishing process
of a test package, including the general informations, responsibilities and descriptions.

		entity TestPackageManifest {

This is the unique id of the test package using an UUID. This id has to be updated
if a new release of the test package is published.

			id: string match /[a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89ab][a-f0-9]{3}-[a-f0-9]{12}/

This is the unique name of the test package following the P23R NDRs. The test set's name
should never be updated even if a new release is published.

			name: string

The title is a short description of the intention of the test package.

			title: string

This is the release number of this test package following the P23R NDRs.

			release: cc:Release

This is the date and time when the current test package was assembled.

			releasedAt: dateTime

The subject describes the content of the test set semantically for search purposes.
Each subject must contain one keyword.

			subjects : string many optional

These are the editors and organisations responsible for designing and maintaining this test package.

			creators: string many

This is the name of the provider distributing this test package. Usually this
should be the public or an enterprise control centre.

			publisher: string

These descriptions describe the content of the test package shown to the adminstrator of the P23R.

			descriptions: cc:Description many

This description should contain any legal and copyright information of the test
package shown to the adminstrator of the P23R.

			licenses : cc:Description many optional
			
		}
	}
