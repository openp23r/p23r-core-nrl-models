## Specification of the Model Package Archive Structure

### _p23r.nrl.ModelPackageStructure_

This specification describes the file and directory structure of the model packages, which is used to generate the archive file (zip) of a model package.

#### Notes

_none_

#### Specification

The model package includes all XSD schemata to access the data from the source applications of the pincipal.

This directory must be the unique name of the model package as given in the manifest of the model package.

	«ModelPackageName»

This directory must be the unique release number of the model package as given in the manifest
of the model package.

		«Release»

This optional directory contain the source files, if the source file is written in one of the optional
F-BRS languages.

			sources → optional

The manifest of the rule package contains its meta information regarding especially the packing and
publishing aspects of test package.

			Manifest.xml

These are supporting schemata, which are shared by all data models.

			«support».xsd

Each data model have its own subdirectory. The name of the directory must be the unique name of the
model as given in the manifest of the model. The names _shared_ and _sources_ are reserved.

			«Model Name» → many

This optional directory contain the source files, if the source file is written in one of the optional
F-BRS languages.

				sources → optional

The manifest of the model contains its meta information regarding especially the administrative
information and overall aspects.

				Manifest.xml

This is the XSD schema defining the target data model. The schema can use supporting XSD schemata.

				«schema».xsd
				«support».xsd → optional many

This document is a readable, formated ebook with all commented source files of the model.

				Source.epub → optional
