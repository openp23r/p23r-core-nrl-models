# Models for the P23R Notification Rule Language

All schemata used in the notification rule packages, model packages, test
packages and more by the P23R depot are described as P23R models. The generated
XSD schemata are also included.

The models are specified and explained in the related specification documents,
especially the `P23R: Spezifikation der Technischen Benachrichtigungsregelsprache`.

The directory `current` contains all current data models while `previous`
contains the outdated and deprecated data models.

---

This project is part of the *openP23R* initiative. All related projects
are listed at https://gitlab.com/openp23r/openp23r .

The legal conditions see *LICENSE* file.

The project is maintained by P23R-Team (a) Fraunhofer FOKUS
