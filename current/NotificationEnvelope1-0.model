### Specification of the Notification Envelope

#### _p23r.nrl.NotificationEnvelope_

This specification describes the model for a standard part of a notification used for all notifications which are compatible with the P23R protocol.

##### Notes

_none_

##### Specification of the model

This model describes a fragment to be part of a
notification compatible with the P23R protocol.
This fragment has mandatory properties which should
not be changed by the signee (part of the so
called notification core).



	model p23r.nrl.NotificationEnvelope as ne release 1.0 {
		
		creator 'Jan Gottschick'
		organisation 'P23R-Team'
		requires p23r release 1.0
		
		title 'Information included in a notification.'
		
		tag 'notification core'
		tag 'notification'
		
		description 'This model describes the information which should be always included in a notification.'
	
		namespace cc=http://leitstelle.p23r.de/NS/p23r/nrl/Common1-0 file 'Common1-0.xsd'
	
		namespace ds=http://www.w3.org/2000/09/xmldsig# file 'xmldsig-core-schema.xsd'
		

This model is an example for notification rules
compatible with the P23R protocol.



		entity NotificationEnvelope {

This is the generated notification which has beenupdated,
approved and signed by the signee.


			notification : Notification

This is the signed hash code for the core part
of the content signed by the P23R. The core
part of the content will be extracted by the
script
'<<Rule Name>>/notificationCore.xslt'

			signedCore : instance of ds:Signature

This is the signed hash code for the whole
content signed by the signee.



			signedContent : instance of ds:Signature
		}
		

The content of a notification is mainly rule
specific.



		entity Notification : * {
		

This is the main part of a notification, which
every notification compatible with the
P23R protocol has to include.


			common : Common
		}
		

The common part of a notification contains
P23R specific and generic information to enable
all unique features of the P23R.



		entity Common {

This is a unique identification of the sender
of this notification. It must be an URI.



			senderId : anyURI optional

This is a unique name of the creator, e.g. the
legal name of the company or organisation.



			creator : string

This is the readable name of the person who has
approved and signed the notification.

			approvedBy : string

This is an email address, where to send
questions, which will be delivered to the
responsible person for this notification.



			questionTos : anyURI match /^mailto:./ many optional
			

This is the unique id of the notification rule
to generate this notification.



			generatedByRuleId : string

If the notification rule to generate this one
was derived from another (public) notification
rule, the unique id of the notification rule,
from which that one was derived should be
included.


			ruleBasedOnRule : string optional
			

This is a (predefined) statement, where the
signee states that he did not changed the core
part of the notification and used the given
notification rules mentioned above.



			legalStatement : string
			

These are the web service addresses, to which
answers and other replies should be sent.



			replyTo : anyURI optional

This is the tenant of the P23R to be specified
in the P23R protocol while replying.



			replyToTenant : string optional
			

This is the internal transaction identifier of
the P23R, which should be included in replies.



			transactionId : string

This is the internal notification identifier of
the P23R for this notification.



			notificationId : string

This is the internal identifier of the P23R,
which generated this notification.



			p23rId : string optional

This is the release number of the P23R
specification, which was implemented by the
P23R product, which generated this
notification.



			p23rSpecification : cc:Release optional
		}
	}
