## Specification of the Test Package Archive Structure

### _p23r.nrl.TestPackageStructure_

This specification describes the file and directory structure of the test packages, which is used to generate the archive file (zip) of a test package.

#### Notes

_none_

#### Specification

The test package includes comprehensive test cases to validate the notification rules.

This directory must be the unique name of the test package as given in the manifest of the test package.


	«TestPackageName»

This directory must be the unique release number of the test package as given in the manifest of the test package.

		«Release»


The manifest of the test package contains its meta information regarding especially the packing and publishing aspects of test package.

			Manifest.xml


Each test set have its own subdirectory. The name of the directory must be the unique name of the test set as given in the manifest of the test set.

			«TestSetName» → many


The manifest of the test set contains its meta information regarding especially the administrative information and overall aspects of the containing test cases, e.g regarding the reviewing and approval process.

				Manifest.xml


The optional _data_ subdirectory contains the test data usable by all test cases.

				data → optional


Each test data file should be named by the namespace of the pivot data model, to which the data belongs. Therefore each test data file must contain only data for one namespace. The test data must be given using the XML format.

					«pivot».xml → many optional


This optional directory contain the source files, if the source file is written in one of the optional F-BRS languages.

				sources → optional


Each test case  have its own subdirectory. The name of the directory must be the unique name of the test case as given in the manifest of the test case.

				«TestCaseName» → many


The manifest of the test case contains its meta information regarding especially the administrative information  of the  test case.

					Manifest.xml


This is the triggering message to start the generation of the notification respectively notifications for testing a notification rule. The schema is described in the section about the web service _IExtMessageDeliverTest_.

					Message.xml


This is the private signature key to sign up a notification in the approval task for a notification. The key is required if the final notification to be compared with is signed, too.

					Signature.pfx → optional

This optional filter will be applied to a generated and approved notification before it will be compared with the expected notification. As there can be more than one notification generated, e.g. applying the multi notification scripts, each filter must correspond to the expected notification using the same suffix in the file name.

					Filter-«Number».xslt → many optional


This are the expection notification respectively notifications. As there can be more than one notification generated, e.g. applying the multi notification scripts, each notification can have a corresponding filter by using the same suffix in the file name. If there are multiple expected notifications each generated notification must match a corresponding expected notification in a pairwise fashion. So no notification should not be matched, but also two generated notification can not match the same expected notification. 

					Notification-«Number».xml → many


The optional _data_ subdirectory contains the specific test data for the test case.

					data → optional


Each test data file should be named by the namespace of the pivot data model, to which the data belongs. Therefore each test data file must contain only data for one namespace. The test data must be given using the XML format. If the surrounding test set contains test data with the same namespace the test data from the test set will be overwritten by the more specific test data from the test case.

						«pivot».xml → many optional


This optional directory contain the source files, if the source file is written in one of the optional F-BRS languages.

					sources → optional
